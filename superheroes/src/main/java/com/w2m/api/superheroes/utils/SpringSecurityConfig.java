package com.w2m.api.superheroes.utils;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    	PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	        auth.inMemoryAuthentication()
	                .withUser("admin").password(encoder.encode("password"))
	                   .roles(Rol.ADMIN.name()).and()
	                .withUser("user").password(encoder.encode("password"))
	                   .roles(Rol.USER.name());
	    }
	    
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	          
	    	 http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	    	    .and().csrf().disable()
	    	    .authorizeRequests().antMatchers(HttpMethod.DELETE, "/api/superheroes/delete/*").hasRole(Rol.ADMIN.name())
	    	    					.antMatchers(HttpMethod.GET, "/api/superheroes/*").authenticated()
	    	                        .and()
	    	    	                .authorizeRequests().antMatchers("/console/**").permitAll()
	    	    .and().httpBasic();
	    	  http.headers().frameOptions().disable();
	    	  http.formLogin();
	    	
	    	  
	    	
	    }
	      
	    
}

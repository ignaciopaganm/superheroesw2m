package com.w2m.api.superheroes.utils;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CronoAspect {

	    @Around("@annotation(Crono)")
	    public Object trace(ProceedingJoinPoint joinPoint) throws Throwable {
	 
	    	long inicio = System.currentTimeMillis();
	        Object result = joinPoint.proceed();
	    	long fin = System.currentTimeMillis();
	    	long resultado = fin - inicio;
	    	String segundos=null;
	    	if(resultado>1000) 
	    	{
	    		segundos= (DurationFormatUtils.formatDuration(resultado,"s.S"));
		    	System.out.println("El método "+ joinPoint.getSignature() +" ha tardado en ejecutarse "+ segundos +" s");	 
	    	}
	    	System.out.println("El método "+ joinPoint.getSignature() +" ha tardado en ejecutarse "+ resultado +" ms");	 
	        return result;
	    }
	 
	}


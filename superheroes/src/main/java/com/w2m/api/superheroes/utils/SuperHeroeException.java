package com.w2m.api.superheroes.utils;

public class SuperHeroeException extends RuntimeException {

	private static final long serialVersionUID = 2814630611152852943L;

		public SuperHeroeException(String descripcion, Throwable cause) {
			super(descripcion, cause);
		}

		public SuperHeroeException(Throwable cause) {
			super(cause.getMessage() != null ? cause.getMessage() : "", cause);
		}


		public SuperHeroeException(String descripcion) {
			super(descripcion);
		}

		public SuperHeroeException() {
			super();
		}

	}



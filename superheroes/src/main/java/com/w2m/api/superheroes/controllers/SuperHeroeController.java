package com.w2m.api.superheroes.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.w2m.api.superheroes.apimodel.SuperHeroe;
import com.w2m.api.superheroes.services.SuperHeroeService;

@RestController
@RequestMapping("/api/superheroes")
public class SuperHeroeController {

	@Autowired
	private SuperHeroeService superHeroeService;
	
	
	@GetMapping()
	public ResponseEntity<List<SuperHeroe>> getSuperHeroes() {
		ResponseEntity<List<SuperHeroe>> response = null;
		List<SuperHeroe> superHeroes = new ArrayList<>();
		
		try {
			superHeroes = this.superHeroeService.getSuperHeroes();
			if (superHeroes.isEmpty()) 
			{	response = new ResponseEntity<>(HttpStatus.NO_CONTENT);} 
			else 
			{	response = new ResponseEntity<>(superHeroes, HttpStatus.OK);}
		}
		catch (final Exception e) {
			response = tratarError(e);
		}
		
		return response;		
		
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<List<SuperHeroe>> getSuperHeroes(@PathVariable String name)
	{
		ResponseEntity<List<SuperHeroe>> response = null;
		List<SuperHeroe> superHeroes = new ArrayList<>();
		try {
			superHeroes = this.superHeroeService.getSuperHeroes(name);
			if (superHeroes.isEmpty()) 
			{	response = new ResponseEntity<>(HttpStatus.NO_CONTENT);} 
			else 
			{	response = new ResponseEntity<>(superHeroes, HttpStatus.OK);}
		}
		catch (final Exception e) {
			response = tratarError(e);
		}
		
		return response;

	}
	
	@GetMapping("/superheroe/{id}")
	public ResponseEntity<SuperHeroe> getSuperHeroe(@PathVariable Long id)
	{
		ResponseEntity<SuperHeroe> response = null;
		SuperHeroe superHeroe = new SuperHeroe();
		try
		{
			superHeroe = this.superHeroeService.getSuperHeroe(id);
			response = new ResponseEntity<>(superHeroe, HttpStatus.OK);

		}
		catch (final Exception e) {
			response = tratarError(e);
		}

		return response;
		}

	
	
	@PostMapping("/modify")
	public ResponseEntity<SuperHeroe>  modifySuperHeroe(@RequestBody SuperHeroe superHeroe)
	{
		ResponseEntity<SuperHeroe> response = null;
		SuperHeroe superHeroeModified = new SuperHeroe();
		try
		{
			superHeroeModified = this.superHeroeService.modifySuperHeroe(superHeroe);
			response = new ResponseEntity<>(superHeroeModified, HttpStatus.OK);

		}
		catch (final Exception e) {
			response = tratarError(e);
		}
		

		return response;
		
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteSuperHeroe(@PathVariable Long id)
	{
		this.superHeroeService.deleteSuperHeroe(id);
	}
	
	public ResponseEntity tratarError(Exception e)
	{
		System.out.println(e.getMessage());
		ResponseEntity response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		return response;
	}
	
}

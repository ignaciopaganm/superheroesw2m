package com.w2m.api.superheroes.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.w2m.api.superheroes.apimodel.SuperHeroe;

@Repository
public interface SuperHeroeRepository extends JpaRepository<SuperHeroe, Long>{

	public List<SuperHeroe> findByName(String name);
	public Optional<SuperHeroe> findById(Long id);


}

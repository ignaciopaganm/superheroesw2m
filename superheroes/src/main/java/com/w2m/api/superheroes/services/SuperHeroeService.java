package com.w2m.api.superheroes.services;

import java.util.List;

import com.w2m.api.superheroes.apimodel.SuperHeroe;

public interface SuperHeroeService {

		SuperHeroe getSuperHeroe(Long id);
		
		List<SuperHeroe> getSuperHeroes();
		
		List<SuperHeroe> getSuperHeroes(String name);
		
		SuperHeroe modifySuperHeroe (SuperHeroe superHeroe);
		
		void deleteSuperHeroe(Long id);

		
	

}

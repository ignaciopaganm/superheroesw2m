package com.w2m.api.superheroes.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.w2m.api.superheroes.apimodel.SuperHeroe;
import com.w2m.api.superheroes.repositories.SuperHeroeRepository;
import com.w2m.api.superheroes.utils.Crono;
import com.w2m.api.superheroes.utils.SuperHeroeException;

@Service
public class SuperHeroeServiceImpl implements SuperHeroeService{

	@Autowired
	private SuperHeroeRepository superHeroeRepository;
	
	
	@Crono
	@Override
	@Cacheable(cacheNames="superheroes")
	public SuperHeroe getSuperHeroe(Long id) {
		try 
		{	
			Optional<SuperHeroe> superHeroe = this.superHeroeRepository.findById(id);
			return superHeroe.get();
		}
		catch (final Exception e) {
			final SuperHeroeException e1 = new SuperHeroeException(e);
			throw e1;
		}
	}
	

	@Crono
	@Override
	public List<SuperHeroe> getSuperHeroes() {
		try 
		{
			List <SuperHeroe> superHeroes = new ArrayList<>();
			this.superHeroeRepository.findAll().forEach(superHeroes::add);
			return superHeroes;
		}
		catch (final Exception e) {
			final SuperHeroeException e1 = new SuperHeroeException(e);
			throw e1;
		}
	}

	
	@Crono
	@Override
	public List<SuperHeroe> getSuperHeroes(String name) {
		List<SuperHeroe> superHeroesName = new ArrayList<>();
		try
		{
			List<SuperHeroe> superHeroes = this.getSuperHeroes();
			if (superHeroes!=null && !superHeroes.isEmpty())
			{
				for (SuperHeroe s : superHeroes)
				{
					if (StringUtils.containsIgnoreCase(s.getName(), name)) superHeroesName.add(s);
				}
				
				return superHeroesName;
			}
			return null;
		}
		catch (final Exception e) {
			final SuperHeroeException e1 = new SuperHeroeException(e);
			throw e1;
		}
	
	}

	@Crono
	@Override
	@CachePut(cacheNames="superheroes", key="#newSuperHeroe.id")
	public SuperHeroe modifySuperHeroe(SuperHeroe newSuperHeroe) {
		superHeroeRepository.save(newSuperHeroe);
		return newSuperHeroe;
	}

	@Crono
	@Override
	public void deleteSuperHeroe(Long id) {
		try {this.superHeroeRepository.deleteById(id);}
		catch (final Exception e) {
			final SuperHeroeException e1 = new SuperHeroeException(e);
			throw e1;
		}
		
	}

}

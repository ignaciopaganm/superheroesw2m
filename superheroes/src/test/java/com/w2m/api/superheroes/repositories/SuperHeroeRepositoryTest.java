package com.w2m.api.superheroes.repositories;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.w2m.api.superheroes.apimodel.SuperHeroe;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
public class SuperHeroeRepositoryTest {

		
		@Autowired
		private SuperHeroeRepository superHeroeRepository;
		  
		    @Test
		    public void superHeroeRepositoryTest(){
		    	SuperHeroe s = new SuperHeroe();
		    	s.setName("black panther");
		        superHeroeRepository.save(s);
		        SuperHeroe superHeroeDB = superHeroeRepository.findById(s.getId()).get();
		        assertThat(superHeroeDB).isNotNull();
		    }
	}


package com.w2m.api.superheroes.services.integracion;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.w2m.api.superheroes.apimodel.SuperHeroe;
import com.w2m.api.superheroes.repositories.SuperHeroeRepository;

@DataJpaTest
public class SuperHeroeServiceItTest {
	
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private SuperHeroeRepository superHeroeRepository;
	
	 @Test
	  public void whenFindByName_thenReturnSuperHeroe() {
	    SuperHeroe superHeroe = new SuperHeroe();
	    superHeroe.setName("spiderman");
	    superHeroe.setPower("lanzar telarañas");
	    superHeroe.setRealName("peter parker");
	    entityManager.persist(superHeroe);
	    entityManager.flush();
	    
	    try 
	    {
	    	List<SuperHeroe> lista = superHeroeRepository.findByName("spiderman");
	    	 SuperHeroe s = lista.get(0);
	 	    assertThat(s.getName())
	 	        .isEqualTo(superHeroe.getName());
	    }
	    catch (Exception e) {throw e;}   
	  }
}

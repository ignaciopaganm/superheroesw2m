package com.w2m.api.superheroes.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.w2m.api.superheroes.apimodel.SuperHeroe;
import com.w2m.api.superheroes.services.SuperHeroeService;

@ExtendWith(MockitoExtension.class)
public class SuperHeroeControllerTest {

	@Mock
	SuperHeroeService superHeroeService;
	
	@InjectMocks
	SuperHeroeController superHeroeController = new SuperHeroeController();
	
	@Test
	void getSuperHeroesTest()
	{
		Mockito.when(this.superHeroeService.getSuperHeroes()).thenReturn(new ArrayList<>());
		final ResponseEntity<List<SuperHeroe>> response = this.superHeroeController
				.getSuperHeroes();
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}
	

	
}

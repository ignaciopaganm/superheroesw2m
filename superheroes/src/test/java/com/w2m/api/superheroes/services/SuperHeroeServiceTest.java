package com.w2m.api.superheroes.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.w2m.api.superheroes.apimodel.SuperHeroe;
import com.w2m.api.superheroes.repositories.SuperHeroeRepository;
import com.w2m.api.superheroes.utils.SuperHeroeException;

@ExtendWith(MockitoExtension.class)
public class SuperHeroeServiceTest {
	
	
	@Mock
	private SuperHeroeRepository superHeroeRepository;
	
	@InjectMocks
	private SuperHeroeService superHeroeService = new SuperHeroeServiceImpl();

	@Mock
	private SuperHeroe superHeroe;
	
	
	  @Before
	    public void setup(){
		  SuperHeroe ironMan = new SuperHeroe();
			ironMan.setName("ironMan");
			ironMan.setPower("Volar");
			ironMan.setRealName("Tony Stark");
			superHeroeRepository.save(ironMan);
	    }

	@Test
	void getSuperHeroesTest()
	{
		List<SuperHeroe> superHeroesMock = generarSuperHeroes();
		Mockito.when(superHeroeRepository.findAll()).thenReturn(superHeroesMock);
		List<SuperHeroe> superHeroes = superHeroeService.getSuperHeroes();
		assertEquals(superHeroes,superHeroesMock);
	}
	
	
	
	@Test
	void getSuperHeroeExceptionTest()
	{
		NullPointerException e = new NullPointerException();
		Mockito.when(superHeroeRepository.findById(Mockito.any(Long.class))).thenThrow(e);
		assertThrows(SuperHeroeException.class, () -> {
			this.superHeroeService.getSuperHeroe((long)100);
		});
		
	}
	

	private List<SuperHeroe> generarSuperHeroes()
	{
		List <SuperHeroe> superHeroes = new ArrayList<>();
		 SuperHeroe ironMan = new SuperHeroe();
			ironMan.setName("ironMan");
			ironMan.setPower("Volar");
			ironMan.setRealName("Tony Stark");
			superHeroeRepository.save(ironMan);
			superHeroes.add(ironMan);
		return superHeroes;
	}
	  
}
